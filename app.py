from PySide2.QtWidgets import QApplication
from controllers.main_window import MainForm


if __name__== "__main__":
    app = QApplication()
    window = MainForm()
    window.show()

    app.exec_()