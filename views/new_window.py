# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'new_window.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class NewFileWindow(object):
    def setupUi(self, NewFileWindow):
        if not NewFileWindow.objectName():
            NewFileWindow.setObjectName(u"NewFileWindow")
        NewFileWindow.resize(405, 466)
        palette = QPalette()
        brush = QBrush(QColor(0, 0, 0, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(85, 170, 255, 255))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        brush2 = QBrush(QColor(212, 234, 255, 255))
        brush2.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Light, brush2)
        brush3 = QBrush(QColor(148, 202, 255, 255))
        brush3.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Midlight, brush3)
        brush4 = QBrush(QColor(42, 85, 127, 255))
        brush4.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Dark, brush4)
        brush5 = QBrush(QColor(57, 113, 170, 255))
        brush5.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Active, QPalette.Text, brush)
        brush6 = QBrush(QColor(255, 255, 255, 255))
        brush6.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.BrightText, brush6)
        palette.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush6)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette.setBrush(QPalette.Active, QPalette.Shadow, brush)
        brush7 = QBrush(QColor(170, 212, 255, 255))
        brush7.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.AlternateBase, brush7)
        brush8 = QBrush(QColor(255, 255, 220, 255))
        brush8.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.ToolTipBase, brush8)
        palette.setBrush(QPalette.Active, QPalette.ToolTipText, brush)
        brush9 = QBrush(QColor(0, 0, 0, 128))
        brush9.setStyle(Qt.SolidPattern)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Active, QPalette.PlaceholderText, brush9)
#endif
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Light, brush2)
        palette.setBrush(QPalette.Inactive, QPalette.Midlight, brush3)
        palette.setBrush(QPalette.Inactive, QPalette.Dark, brush4)
        palette.setBrush(QPalette.Inactive, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette.setBrush(QPalette.Inactive, QPalette.BrightText, brush6)
        palette.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush6)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Shadow, brush)
        palette.setBrush(QPalette.Inactive, QPalette.AlternateBase, brush7)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipBase, brush8)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipText, brush)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Inactive, QPalette.PlaceholderText, brush9)
#endif
        palette.setBrush(QPalette.Disabled, QPalette.WindowText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Light, brush2)
        palette.setBrush(QPalette.Disabled, QPalette.Midlight, brush3)
        palette.setBrush(QPalette.Disabled, QPalette.Dark, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Disabled, QPalette.Text, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.BrightText, brush6)
        palette.setBrush(QPalette.Disabled, QPalette.ButtonText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Shadow, brush)
        palette.setBrush(QPalette.Disabled, QPalette.AlternateBase, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipBase, brush8)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipText, brush)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Disabled, QPalette.PlaceholderText, brush9)
#endif
        NewFileWindow.setPalette(palette)
        self.label = QLabel(NewFileWindow)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(10, 20, 381, 20))
        self.label.setFrameShape(QFrame.Box)
        self.label_2 = QLabel(NewFileWindow)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(30, 80, 61, 13))
        self.nameLineEdit = QLineEdit(NewFileWindow)
        self.nameLineEdit.setObjectName(u"nameLineEdit")
        self.nameLineEdit.setGeometry(QRect(30, 100, 271, 20))
        self.label_3 = QLabel(NewFileWindow)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(30, 140, 61, 13))
        self.label_4 = QLabel(NewFileWindow)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(30, 200, 61, 13))
        self.label_5 = QLabel(NewFileWindow)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(30, 260, 61, 13))
        self.extLineEdit = QLineEdit(NewFileWindow)
        self.extLineEdit.setObjectName(u"extLineEdit")
        self.extLineEdit.setGeometry(QRect(30, 160, 271, 20))
        self.ownerLineEdit = QLineEdit(NewFileWindow)
        self.ownerLineEdit.setObjectName(u"ownerLineEdit")
        self.ownerLineEdit.setGeometry(QRect(30, 220, 271, 20))
        self.selFileLineEdit = QLineEdit(NewFileWindow)
        self.selFileLineEdit.setObjectName(u"selFileLineEdit")
        self.selFileLineEdit.setGeometry(QRect(30, 340, 211, 20))
        self.label_6 = QLabel(NewFileWindow)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(30, 320, 61, 13))
        self.selFileButton = QPushButton(NewFileWindow)
        self.selFileButton.setObjectName(u"selFileButton")
        self.selFileButton.setGeometry(QRect(240, 330, 61, 31))
        self.selFileButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.selFileButton.setStyleSheet(u"QPushButton:hover\n"
"{\n"
"	border-style: solid;\n"
"   	background-color:#bbdefb;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed\n"
"{\n"
"	 	background-color:#0069c0;\n"
"}")
        icon = QIcon()
        icon.addFile(u"./assests/icons/Document-Preview-icon.png", QSize(), QIcon.Normal, QIcon.Off)
        self.selFileButton.setIcon(icon)
        self.selFileButton.setIconSize(QSize(30, 30))
        self.selFileButton.setFlat(True)
        self.addFileButton = QPushButton(NewFileWindow)
        self.addFileButton.setObjectName(u"addFileButton")
        self.addFileButton.setGeometry(QRect(90, 400, 91, 41))
        self.addFileButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.addFileButton.setStyleSheet(u"QPushButton\n"
"{	\n"
"	height: 2em;\n"
" 	border-style: solid;\n"
"	border-width: 2px;\n"
" 	border-color: #0069c0;\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"   	background-color:#0069c0;\n"
"	color:white;\n"
"}")
        icon1 = QIcon()
        icon1.addFile(u"./assests/icons/File-New-icon.png", QSize(), QIcon.Normal, QIcon.Off)
        self.addFileButton.setIcon(icon1)
        self.addFileButton.setIconSize(QSize(35, 35))
        self.addFileButton.setFlat(True)
        self.cancelFileButton = QPushButton(NewFileWindow)
        self.cancelFileButton.setObjectName(u"cancelFileButton")
        self.cancelFileButton.setGeometry(QRect(190, 400, 91, 41))
        self.cancelFileButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.cancelFileButton.setStyleSheet(u"QPushButton\n"
"{	\n"
"	height: 2em;\n"
" 	border-style: solid;\n"
"	border-width: 2px;\n"
" 	border-color: grey;\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"   	background-color:grey;\n"
"	color:white;\n"
"}")
        self.cancelFileButton.setIconSize(QSize(35, 35))
        self.cancelFileButton.setFlat(True)
        self.visLineEdit = QLineEdit(NewFileWindow)
        self.visLineEdit.setObjectName(u"visLineEdit")
        self.visLineEdit.setGeometry(QRect(30, 280, 271, 20))

        self.retranslateUi(NewFileWindow)

        QMetaObject.connectSlotsByName(NewFileWindow)
    # setupUi

    def retranslateUi(self, NewFileWindow):
        NewFileWindow.setWindowTitle(QCoreApplication.translate("NewFileWindow", u"New File", None))
        self.label.setText(QCoreApplication.translate("NewFileWindow", u"<html><head/><body><p align=\"center\"><span style=\" font-size:11pt; font-weight:600;\">NEW FILE</span></p></body></html>", None))
        self.label_2.setText(QCoreApplication.translate("NewFileWindow", u"<html><head/><body><p><span style=\" font-weight:600;\">File Name</span></p></body></html>", None))
        self.label_3.setText(QCoreApplication.translate("NewFileWindow", u"<html><head/><body><p><span style=\" font-weight:600;\">Extension</span></p></body></html>", None))
        self.label_4.setText(QCoreApplication.translate("NewFileWindow", u"<html><head/><body><p><span style=\" font-weight:600;\">File Owner</span></p></body></html>", None))
        self.label_5.setText(QCoreApplication.translate("NewFileWindow", u"<html><head/><body><p><span style=\" font-weight:600;\">Visibility</span></p></body></html>", None))
        self.ownerLineEdit.setText("")
        self.label_6.setText(QCoreApplication.translate("NewFileWindow", u"<html><head/><body><p><span style=\" font-weight:600;\">Select File</span></p></body></html>", None))
        self.selFileButton.setText("")
        self.addFileButton.setText(QCoreApplication.translate("NewFileWindow", u"ADD", None))
        self.cancelFileButton.setText(QCoreApplication.translate("NewFileWindow", u"CANCEL", None))
        self.visLineEdit.setText(QCoreApplication.translate("NewFileWindow", u"Public", None))
    # retranslateUi

