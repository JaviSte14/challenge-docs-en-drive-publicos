import sqlite3
from sqlite3 import Error
from .connection import create_connection


def insert_file(data):
    conn = create_connection()
    sql = """ INSERT INTO files (file_name, file_owner, extension, visibility, file_path, last_modification)
                VALUES(?, ?, ?, ?, ?, datetime('now', 'localtime'))
    """

    try: 
        cur = conn.cursor()
        cur.execute(sql, data)
        conn.commit()
        print("New file added!")
        return True
    except Error as e:
        print("Error inserting new file! " + str(e))
    finally:
        if conn:
            cur.close()
            conn.close()

def update_file(_id, data):
    conn = create_connection()
    sql = f"""UPDATE files SET
                            file_name = ?,
                            file_owner = ?,
                            extension = ?,
                            visibility = ?,
                            file_path = ?,
                            last_modification = datetime('now', 'localtime')
            WHERE file_id = {_id}  

    """

    try:
        cur = conn.cursor()
        cur.execute(sql, data)
        conn.commit()
        print("File updated!")
        return True
    except Error as e:
        print("Error updating file! " + str(e))
    finally:
        if conn:
            cur.close()
            conn.close()

def delete_file(_id):
    conn = create_connection()
    sql = f"DELETE FROM files WHERE file_id = {_id}"

    try:
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
        print("File deleted!")
        return True
    except Error as e:
        print("Error deleting file! " + str(e))
    finally:
        if conn:
            cur.close()
            conn.close()

def select_all_files():
    conn = create_connection()
    sql = "SELECT * FROM files"

    try:
        cur = conn.cursor()
        cur.execute(sql)
        files = cur.fetchall()
        return files
    except Error as e:
        print("Error selecting all files! " + str(e))
    finally:
        if conn:
            cur.close()
            conn.close()

def select_file_by_id(_id):
    conn = create_connection()
    sql = f"SELECT * FROM files WHERE file_id = {_id}"

    try:
        cur = conn.cursor()
        cur.execute(sql)
        files = cur.fetchone()
        return files
    except Error as e:
        print("Error selecting file by id! " + str(e))
    finally:
        if conn:
            cur.close()
            conn.close()

def select_file_by_name(file_name):
    conn = create_connection()
    sql = f"SELECT * FROM files WHERE file_name LIKE '%{file_name}%'"

    try:
        cur = conn.cursor()
        cur.execute(sql)
        files = cur.fetchall()
        return files
    except Error as e:
        print("Error selecting file by name! " + str(e))
    finally:
        if conn:
            cur.close()
            conn.close()

def select_file_by_extension(extension):
    conn = create_connection()
    sql = f"SELECT * FROM files WHERE extension LIKE '%{extension}%'"

    try:
        cur = conn.cursor()
        cur.execute(sql)
        files = cur.fetchall()
        return files
    except Error as e:
        print("Error selecting file by extension! " + str(e))
    finally:
        if conn:
            cur.close()
            conn.close()

def select_file_by_visibility(visibility):
    conn = create_connection()
    sql = f"SELECT * FROM files WHERE visibility LIKE '%{visibility}%'"

    try:
        cur = conn.cursor()
        cur.execute(sql)
        files = cur.fetchall()
        return files
    except Error as e:
        print("Error selecting file by visibility! " + str(e))
    finally:
        if conn:
            cur.close()
            conn.close()