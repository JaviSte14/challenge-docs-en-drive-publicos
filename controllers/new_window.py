from PySide2.QtWidgets import QWidget, QFileDialog
from PySide2.QtCore import Qt
from views.new_window import NewFileWindow
from db.drive import insert_file
import shutil
import os

class NewFileForm(QWidget, NewFileWindow):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setupUi(self)
        self.setWindowFlag(Qt.Window)

        self.addFileButton.clicked.connect(self.add_file)
        self.selFileButton.clicked.connect(self.select_file)
        self.cancelFileButton.clicked.connect(self.close)
        
    def check_inputs(self):
        file_name = self.nameLineEdit.text()
        file_owner = self.ownerLineEdit.text()
        extension = self.extLineEdit.text()
        visibility = self.visLineEdit.text()
        file_path = self.selFileLineEdit.text()

        errors_count = 0
        
        if file_name == "":
            print("File name field is mandatory!")
            errors_count += 1
        if file_owner == "":
            print("File owner field is mandatory!")
            errors_count += 1
        if extension == "":
            print("Extension field is mandatory!")
            errors_count += 1  
        if visibility == "":
            print("Visibility field is mandatory!")
            errors_count += 1        
        if file_path == "":
            print("You must select a file!")
            errors_count += 1   

        if errors_count == 0:
            return True


    def add_file(self):
        file_name = self.nameLineEdit.text()
        file_owner = self.ownerLineEdit.text()
        extension = self.extLineEdit.text()
        visibility = self.visLineEdit.text()
        file_path = self.selFileLineEdit.text()

        if self.check_inputs():
            new_path = shutil.copy(file_path, "files_history")
            data = (file_name, file_owner, extension, visibility, new_path)
            if insert_file(data):
                self.clean_inputs()
            else:
                self.selFileLineEdit.setText(new_path)
                self.undo()

    def clean_inputs(self):
        self.nameLineEdit.clear()
        self.ownerLineEdit.clear()
        self.extLineEdit.clear()
        self.visLineEdit.clear()
        self.selFileLineEdit.clear()

    def select_file(self):
        path = QFileDialog.getOpenFileName()[0]
        self.selFileLineEdit.setText(path)

    def undo(self):
        path = self.selFileLineEdit.text

        if path != "":
            os.remove(path)