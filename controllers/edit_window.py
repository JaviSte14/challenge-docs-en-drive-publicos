from PySide2.QtWidgets import QWidget, QFileDialog
from PySide2.QtCore import Qt
from views.edit_window import EditFileWindow
from db.drive import select_file_by_id, update_file
import re
import os
import shutil

class EditFileForm(QWidget, EditFileWindow):

    def __init__(self, parent=None, _id=None):
        self._id = _id
        super().__init__(parent)

        self.setupUi(self)
        self.setWindowFlag(Qt.Window)
        self.populate_inputs()
        self.selFileButton.clicked.connect(self.select_file)
        self.editFileButton.clicked.connect(self.edit_file)
        self.cancelFileButton.clicked.connect(self.close)

    def populate_inputs(self):
        data = select_file_by_id(self._id)
        
        self.nameLineEdit.setText(data[1])
        self.extLineEdit.setText(data[2])
        self.ownerLineEdit.setText(data[3])
        self.visLineEdit.setText(data[4])
        self.selFileLineEdit.setText(data[5])

    def select_file(self):
        self.old_path = self.selFileLineEdit.text()
        path = QFileDialog.getOpenFileName()[0]
        self.selFileLineEdit.setText(path)

    def check_inputs(self):
        file_name = self.nameLineEdit.text()
        file_owner = self.ownerLineEdit.text()
        extension = self.extLineEdit.text()
        visibility = self.visLineEdit.text()
        file_path = self.selFileLineEdit.text()

        errors_count = 0
        
        if file_name == "":
            print("File name field is mandatory!")
            errors_count += 1
        if file_owner == "":
            print("File owner field is mandatory!")
            errors_count += 1
        if extension == "":
            print("Extension field is mandatory!")
            errors_count += 1        
        if visibility == "":
            print("Visibility field is mandatory!")
            errors_count += 1  
        if file_path == "":
            print("You must select a file!")
            errors_count += 1   

        if errors_count == 0:
            return True

    def edit_file(self):
        file_name = self.nameLineEdit.text()
        file_owner = self.ownerLineEdit.text()
        extension = self.extLineEdit.text()
        visibility = self.visLineEdit.text()
        file_path = self.selFileLineEdit.text()

        data = [file_name, file_owner, extension, visibility, file_path]

        if self.check_inputs():
            path_to_check = "files_history\\" + re.split("/|\\\\", file_path)[-1]
            result = os.path.exists(path_to_check)

            if result == False:
                data[4] = shutil.copy(file_path, "files_history")
                os.remove(self.old_path)

            if update_file(self._id, data):
                self.close()


