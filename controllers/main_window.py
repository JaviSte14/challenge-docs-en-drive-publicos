from PySide2.QtWidgets import QWidget, QTableWidgetItem, QAbstractItemView
from views.main_window import MainWindow
from db.drive import select_all_files, select_file_by_name, select_file_by_extension, select_file_by_visibility, delete_file
import os



class MainForm(QWidget, MainWindow):

    def __init__(self):
        super().__init__()

        self.setupUi(self)
        self.addFile.clicked.connect(self.open_new_window)
        self.editFile.clicked.connect(self.open_edit_window)
        self.table_config()
        self.populate_table(select_all_files())
        self.populate_combobox()
        self.refreshButton.clicked.connect(lambda: self.populate_table(select_all_files()))
        self.openFile.clicked.connect(self.open_file)
        self.searchButton.clicked.connect(self.search)
        self.deleteFile.clicked.connect(self.delete_file)

    def open_new_window(self):
        from controllers.new_window import NewFileForm
        window = NewFileForm(self)
        window.show()

    def open_edit_window(self):
        from controllers.edit_window import EditFileForm
        selected_row = self.tableWidget.selectedItems()

        if selected_row:
            file_id = int(selected_row[0].text())
            window = EditFileForm(self, file_id)
            window.show()

        self.tableWidget.clearSelection()

    def open_file(self):
        selected_row = self.tableWidget.selectedItems()

        if selected_row:
            path = selected_row[5].text()
            os.startfile(path)

        self.tableWidget.clearSelection()
                 
    def delete_file(self):
        selected_row = self.tableWidget.selectedItems()

        if selected_row:
            file_id = int(selected_row[0].text())
            row = selected_row[0].row()

            if delete_file(file_id):
                file_path = selected_row[5].text()
                self.tableWidget.removeRow()
                os.remove(file_path)

        self.records_quantity()


    def table_config(self):
        column_headers = ("File ID", "File Name", "File Owner", "Extension", "Visibility", "File Path", "Last Modification")
        self.tableWidget.setColumnCount(len(column_headers))
        self.tableWidget.setHorizontalHeaderLabels(column_headers)

        self.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)

    def populate_table(self, data):
        self.tableWidget.setRowCount(len(data))

        for (index_row, row) in enumerate(data):
            for(index_cell, cell) in enumerate(row):
                self.tableWidget.setItem(index_row, index_cell, QTableWidgetItem(str(cell)))
        self.records_quantity()

    def populate_combobox(self):
        cb_option = ("", "File Name", "Extension", "Visibility")
        self.searchbycomboBox.addItems(cb_option)

    def search_file_by_name(self, file_name):
        data = select_file_by_name(file_name)

        self.populate_table(data)

    def search_file_by_extension(self, extension):
        data = select_file_by_extension(extension)

        self.populate_table(data)

    def search_file_by_visibility(self, visibility):
        data = select_file_by_visibility(visibility)

        self.populate_table(data)

    def search(self):
        option_selected = self.searchbycomboBox.currentText()
        parameter = self.parameterlineEdit.text()

        if option_selected == "":
            print("You must select an option!")
        else:
            if parameter == "":
                print("You must write what you want to consult!")
            else:
                if option_selected == "File Name":
                    self.search_file_by_name(parameter)
                elif option_selected == "Extension":
                    self.search_file_by_extension(parameter)
                elif option_selected == "Visibility":
                    self.search_file_by_visibility(parameter)

    def records_quantity(self):
        qty_rows = str(self.tableWidget.rowCount())
        self.filesQntyLabel.setText(qty_rows)